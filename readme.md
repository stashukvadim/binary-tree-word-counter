Write a Java-based program (command line) that reads text from a file, splits it
into words at spaces and newline characters and constructs an (unbalanced)
binary tree where each leaf node represents a unique word.
The tree construction shall start by creating a node for each unique word, where
a node has a field to keep track of the occurrence count. The algorithm starts
with the two least occurring nodes and creates a parent node. The parent node
gets assigned an occurrence count that is the sum of the word occurrences. The
process then repeats, i.e., it locates the two nodes with the least occurrence
count, creates a parent node, and so on, until all nodes are part of the tree.
Finally, print the tree to the console (very basic output is sufficient).

**Build**
mvn clean package

**Run**
java -jar intellias-code-challenge-binary-tree-wordcounter-1.0.jar <filename>

I've also added an already built jar 
_intellias-code-challenge-binary-tree-wordcounter-1.0.jar_

and a screenshot of the program output.
_run_output.png_ 
