package org.github.stashuk.intellias.codechallenge.wordcounter;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Java-based program (command line) that reads text from a file, splits it
 * into words at spaces and newline characters and constructs an (unbalanced)
 * binary tree where each leaf node represents a unique word.
 */
public class App {
    public static void main(String[] args) throws Exception {
        if (args.length == 0){
            System.err.println("Please provide file name");
            return;
        }

        String filePath = args[0];
        Path path = Paths.get(filePath);
        if (!Files.exists(path)) {
            System.err.println("File " + path + " doesn't exist");
            return;
        }

        Map<String, Long> wordCountMap = new FileWordCounter().getWordCountMap(path);
        WordCountTree wordCountTree = new WordCountTree(wordCountMap);

        System.out.println(wordCountTree.asTreeString());
    }
}
