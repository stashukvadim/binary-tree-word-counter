package org.github.stashuk.intellias.codechallenge.wordcounter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.function.Function;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

/**
 * Reads words from file and counts them.
 */
public class FileWordCounter {
    private static final String SEPARATOR = " ";

    /**
     * @param path a file to read words from
     * @return map with words and corresponding count of occurrences in a file
     * @throws IOException
     */
    public Map<String, Long> getWordCountMap(Path path) throws IOException {
        return Files
                .lines(path)
                .flatMap(s -> stream(s.split(SEPARATOR)))
                .filter(s -> !s.isEmpty())
                .collect(groupingBy(Function.identity(), counting()));
    }
}
