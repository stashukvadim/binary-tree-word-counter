package org.github.stashuk.intellias.codechallenge.wordcounter;

public class Node {
    private final Node left;
    private final Node right;
    private final String word;
    private final long count;

    private Node(Node left, Node right, String word, long count) {
        this.left = left;
        this.right = right;
        this.word = word;
        this.count = count;
    }

    public static Node newWordNode(String word, long count){
        return new Node(null, null, word, count);
    }

    public static Node newCountNode(Node left, Node right, long count){
        return new Node(left, right, null, count);
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }

    public String getWord() {
        return word;
    }

    public long getCount() {
        return count;
    }

    public boolean isWordNode() {
        return word != null;
    }

    @Override
    public String toString() {
        return "Node{" + "left=" + left +
                ", right=" + right +
                ", word='" + word + '\'' +
                ", count=" + count +
                '}';
    }
}
