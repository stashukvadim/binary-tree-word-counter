package org.github.stashuk.intellias.codechallenge.wordcounter;

import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

/**
 * Creates a binary tree from word count map according to the following requirements:
 *
 * The tree construction shall start by creating a node for each unique word, where
 * a node has a field to keep track of the occurrence count. The algorithm starts
 * with the two least occurring nodes and creates a parent node. The parent node
 * gets assigned an occurrence count that is the sum of the word occurrences. The
 * process then repeats, i.e., it locates the two nodes with the least occurrence
 * count, creates a parent node, and so on, until all nodes are part of the tree.
 *
 */
public class WordCountTree {
    private Node rootNode;

    public WordCountTree(Map<String, Long> wordCountMap) {
        Objects.requireNonNull(wordCountMap, "Word count map should not be null");
        if (wordCountMap.isEmpty()) {
            throw new IllegalArgumentException("Word count map should not be empty/");
        }

        PriorityQueue<Node> queue = new PriorityQueue<>(Comparator.comparingLong(Node::getCount));

        for (Map.Entry<String, Long> entry : wordCountMap.entrySet()) {
            queue.add(Node.newWordNode(entry.getKey(), entry.getValue()));
        }

        while (queue.size() > 1) {
            Node left = queue.poll();
            Node right = queue.poll();
            Node parent = Node.newCountNode(left, right, left.getCount() + right.getCount());
            queue.offer(parent);
        }

        rootNode = queue.poll();
    }

    public Node getRootNode() {
        return rootNode;
    }

    /**
     * @return WordCountTree string representation, that looks like this:
     *
     * |	   |-------1 to
     * |-------2
     * |	   |-------1 She
     * 4
     * |-------2 had
     *
     */
    public String asTreeString() {
        StringBuilder stringBuilder = asTreeString(rootNode, 0, new StringBuilder());
        return stringBuilder.toString();
    }

    private StringBuilder asTreeString(Node node, int level, StringBuilder sb) {
        if (node == null) {
            return sb;
        }

        asTreeString(node.getRight(), level + 1, sb);
        if (level != 0) {
            for (int i = 0; i < level - 1; i++) {
                sb.append("|\t");
            }

            if (node.isWordNode()) {
                sb.append("|-------" + node.getCount() + " " + node.getWord() + "\n");
            } else {
                sb.append("|-------" + node.getCount() + "\n");
            }
        } else {
            if (node.isWordNode()) {
                sb.append(node.getCount() + " " + node.getWord() + "\n");
            } else {
                sb.append(node.getCount() + "\n");
            }
        }
        asTreeString(node.getLeft(), level + 1, sb);
        return sb;
    }
}
