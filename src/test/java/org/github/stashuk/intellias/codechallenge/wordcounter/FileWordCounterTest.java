package org.github.stashuk.intellias.codechallenge.wordcounter;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static org.junit.Assert.*;

public class FileWordCounterTest {

    @Test
    public void words() throws IOException, URISyntaxException {
        Map<String, Long> wordCountMap = getWordCountMapForFile("words.txt");

        assertEquals(1L, (long) wordCountMap.get("She"));
        assertEquals(1, (long) wordCountMap.get("to"));
        assertEquals(1, (long) wordCountMap.get("problems"));
        assertEquals(2, (long) wordCountMap.get("had"));
        assertEquals(2, (long) wordCountMap.get("address"));
        assertEquals(5, wordCountMap.size());
    }

    @Test
    public void emptyFile() throws URISyntaxException, IOException {
        Map<String, Long> wordCountMap = getWordCountMapForFile("empty.txt");

        assertTrue(wordCountMap.isEmpty());
    }

    @Test
    public void wordsWithMultipleSpaces_shouldIgnoreEmptyStrings() throws URISyntaxException, IOException {
        Map<String, Long> wordCountMap = getWordCountMapForFile("words_multiple_spaces.txt");

        assertEquals(1L, (long) wordCountMap.get("She"));
        assertEquals(2L, (long) wordCountMap.get("had"));
        assertEquals(2, (long) wordCountMap.size());
        assertEquals(2, wordCountMap.size());
    }

    private Map<String, Long> getWordCountMapForFile(String filePath) throws IOException, URISyntaxException {
        Path path = Paths.get(ClassLoader.getSystemResource(filePath).toURI());
        return new FileWordCounter().getWordCountMap(path);
    }
}
