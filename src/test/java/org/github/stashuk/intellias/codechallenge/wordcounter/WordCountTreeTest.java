package org.github.stashuk.intellias.codechallenge.wordcounter;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class WordCountTreeTest {

    /*
    Assert tree has the following string output
    |		|-------1 to
    |-------2
    |		|-------1 She
    4
    |-------2 had
    * */
    @Test
    public void shouldConstructCorrectTree() {
        Map<String, Long> wordCountMap = new HashMap<>();
        wordCountMap.put("She", 1L);
        wordCountMap.put("to", 1L);
        wordCountMap.put("had", 2L);

        WordCountTree wordCountTree = new WordCountTree(wordCountMap);

        System.out.println(wordCountTree.asTreeString());

        assertEquals("|\t|-------1 to\n" +
                "|-------2\n" +
                "|\t|-------1 She\n" +
                "4\n" +
                "|-------2 had\n", wordCountTree.asTreeString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForEmptyWordCounterMap() {
        Map<String, Long> wordCountMap = new HashMap<>();
        new WordCountTree(wordCountMap);
    }

    @Test
    public void shouldReturnOneNodeTreeForOneWordCounterMapEntry() {
        Map<String, Long> wordCountMap = new HashMap<>();
        wordCountMap.put("A", 1L);

        WordCountTree wordCountTree = new WordCountTree(wordCountMap);
        Node root = wordCountTree.getRootNode();

        assertEquals("A", root.getWord());
        assertEquals("1 A\n", wordCountTree.asTreeString());
    }
}
